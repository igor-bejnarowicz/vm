package com.vm.rest.resetGame;

import com.vm.service.GameService;

import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/resetGame")
public class ResetGameRestEndpoint {
    @Inject
    GameService service;

    @POST
    public Response postResetGame() {
        service.resetGame();
        return Response.ok().build();
    }
}