package com.vm.rest.postRoll;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PostRollRequest {
    @XmlElement Integer pinsKnocked = 10;
}
