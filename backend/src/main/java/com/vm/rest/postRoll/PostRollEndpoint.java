package com.vm.rest.postRoll;

import com.google.gson.Gson;
import com.vm.service.GameService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/postRoll")
public class PostRollEndpoint {

    @Inject
    GameService service;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response postRoll(PostRollRequest requestBody) {

        var results = service.processRoll(requestBody.pinsKnocked);

        Gson gson = new Gson();
        String json = gson.toJson(results);
        return Response.ok(json, MediaType.APPLICATION_JSON).build();
    }
}
