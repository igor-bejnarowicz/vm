package com.vm.service;

import com.vm.model.FrameScore;
import com.vm.model.GameInstance;
import com.vm.model.Roll;

import javax.ejb.EJB;
import java.util.List;

public class GameService {

    @EJB
    GameInstance gameInstance;

    public List<FrameScore> processRoll(Integer pinsKnocked) {

        gameInstance.saveRoll(new Roll(pinsKnocked));
        var frameScores = gameInstance.getUpdatedFrameScores();

        return frameScores;
    }

    public void resetGame() {
        gameInstance.resetGame();
    }
}
