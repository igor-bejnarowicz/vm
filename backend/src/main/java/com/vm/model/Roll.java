package com.vm.model;


public class Roll {
    private Integer pinsKnocked = 0;

    public Roll() {}

    public Roll(Integer pinsKnocked) {
        this.pinsKnocked = pinsKnocked;
    }

    public int getPinsKnocked() {
        return pinsKnocked;
    }

    public void setPinsKnocked(Integer pinsKnocked) {
        this.pinsKnocked = pinsKnocked;
    }
}
