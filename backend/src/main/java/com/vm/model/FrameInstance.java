package com.vm.model;

public class FrameInstance {
    private Integer index;
    private Roll firstRoll;
    private Roll secondRoll;
    private Roll thirdRoll;
    private Integer score;

    public FrameInstance(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return this.index;
    }

    public Roll getFirstRoll() {
        return this.firstRoll;
    }

    public void setFirstRoll(Roll firstRoll) {
        this.firstRoll = firstRoll;
    }

    public Roll getSecondRoll() {
        return this.secondRoll;
    }

    public void setSecondRoll(Roll secondRoll) {
        this.secondRoll = secondRoll;
    }

    public Roll getThirdRoll() {
        return this.thirdRoll;
    }

    public void setThirdRoll(Roll thirdRoll) {
        this.thirdRoll = thirdRoll;
    }

    public Integer getScore() {
        return this.score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }
}
