package com.vm.model;

import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
public class GameInstance {
    private List<FrameInstance> frames = new ArrayList<>(Arrays.asList(new FrameInstance(0)));

    public void saveRoll(Roll newRoll) {
        var currentFrame = getCurrentFrame();

        if (!this.isFrameFinal(currentFrame)) {
            if (currentFrame.getFirstRoll() == null) {
                currentFrame.setFirstRoll(newRoll);
            } else if (currentFrame.getSecondRoll() == null && currentFrame.getFirstRoll().getPinsKnocked() < 10) {
                currentFrame.setSecondRoll(newRoll);
            }

            if (isFrameFinished(currentFrame)) {
                var newFrameIndex = this.frames.size();
                this.frames.add(new FrameInstance(newFrameIndex));
            }
        } else { // Final round
            if (currentFrame.getFirstRoll() == null) {
                currentFrame.setFirstRoll(newRoll);
            } else if (currentFrame.getSecondRoll() == null) {
                currentFrame.setSecondRoll(newRoll);
            } else if (currentFrame.getThirdRoll() == null && this.qualifiesForThirdRoll(currentFrame)) {
                currentFrame.setThirdRoll(newRoll);
            }
        }
    }

    public List<FrameScore> getUpdatedFrameScores() {
        return this.frames.stream()
                .filter(f -> f.getScore() == null) // Only consider frames not yet finalized
                .map(this::getUpdatedFrameScore)
                .filter(fs -> fs != null) // Do not include nulls, that is frames that couldn't have been finalized within this roll
                .collect(Collectors.toList());
    }

    public void resetGame() {
        this.frames = new ArrayList<>(Arrays.asList(new FrameInstance(0)));
    }

    private FrameScore getUpdatedFrameScore(FrameInstance frame) {
        updateFrameScore(frame);
        return frame.getScore() != null ? new FrameScore(frame.getScore(), frame.getIndex()) : null;
    }

    private void updateFrameScore(FrameInstance frame) {
        if (this.isFrameFinal(frame)) {
            updateFinalFrame(frame);
        } else {
            updateRegularFrame(frame);
        }
    }

    private void updateRegularFrame(FrameInstance frame) {
        if (this.isFrameFinished(frame)) {
            if (this.qualifiesAsSpare(frame)) {
                var followingRoll = getOneFollowingRoll(frame);

                if (followingRoll != null) {
                    if (frame.getIndex() == 0) {
                        frame.setScore(frame.getFirstRoll().getPinsKnocked() +
                                frame.getSecondRoll().getPinsKnocked() + followingRoll.getPinsKnocked());
                    } else {
                        var previousScore = getScoreOfPreviousFrame(frame);
                        if (previousScore != null) {
                            frame.setScore(previousScore + frame.getFirstRoll().getPinsKnocked() +
                                    frame.getSecondRoll().getPinsKnocked() + followingRoll.getPinsKnocked());
                        }
                    }
                }
            } else if (this.qualifiesAsStrike(frame)) {
                var followingTwoRolls = getTwoFollowingRolls(frame);

                if (followingTwoRolls.size() == 2) {
                    if (frame.getIndex() == 0) {
                        frame.setScore(frame.getFirstRoll().getPinsKnocked() +
                                followingTwoRolls.get(0).getPinsKnocked() + followingTwoRolls.get(1).getPinsKnocked());
                    } else {
                        var previousScore = getScoreOfPreviousFrame(frame);
                        if (previousScore != null) {
                            frame.setScore(previousScore + frame.getFirstRoll().getPinsKnocked() +
                                    followingTwoRolls.get(0).getPinsKnocked() + followingTwoRolls.get(1).getPinsKnocked());
                        }
                    }
                }
            } else {
                if (frame.getIndex() == 0) {
                    frame.setScore(frame.getFirstRoll().getPinsKnocked() + frame.getSecondRoll().getPinsKnocked());
                } else {
                    var previousScore = getScoreOfPreviousFrame(frame);
                    if (previousScore != null) {
                        frame.setScore(previousScore +
                                frame.getFirstRoll().getPinsKnocked() +
                                frame.getSecondRoll().getPinsKnocked());
                    }
                }
            }
        }
    }

    private void updateFinalFrame(FrameInstance frame) {
        if (this.isFrameFinished(frame)) {
            var previousScore = getScoreOfPreviousFrame(frame);
            var score = previousScore + frame.getFirstRoll().getPinsKnocked() + frame.getSecondRoll().getPinsKnocked();

            if (frame.getThirdRoll() != null) {
                score += frame.getThirdRoll().getPinsKnocked();
            }
            frame.setScore(score);
        }
    }

    private FrameInstance getCurrentFrame() {
        return this.frames.get(this.frames.size() - 1);
    }

    private Roll getOneFollowingRoll(FrameInstance frame) {
        if (frame.getIndex() + 1 < this.frames.size()) {
            var followingFrame = this.frames.get(frame.getIndex() + 1);
            return followingFrame.getFirstRoll();
        }
        return null;
    }

    private List<Roll> getTwoFollowingRolls(FrameInstance frame) {
        var firstFollowingRoll = this.getOneFollowingRoll(frame);

        if (firstFollowingRoll != null) {
            if (firstFollowingRoll.getPinsKnocked() < 10 || frame.getIndex() == 8) { // The second to last frame (9th, index = 8) must source both of its 'following rolls' from the next (final) frame, even if the first following roll was a strike
                var secondFollowingRoll = this.frames.get(frame.getIndex() + 1).getSecondRoll();

                if (secondFollowingRoll != null) {
                    return Arrays.asList(firstFollowingRoll, secondFollowingRoll);
                }
            } else {
                if (frame.getIndex() + 2 < this.frames.size()) {
                    var frameTwoFramesAhead = this.frames.get(frame.getIndex() + 2);
                    var secondFollowingRoll = frameTwoFramesAhead.getFirstRoll();

                    if (secondFollowingRoll != null) {
                        return Arrays.asList(firstFollowingRoll, secondFollowingRoll);
                    }
                }
            }
        }

        return new ArrayList<>();
    }

    private Integer getScoreOfPreviousFrame(FrameInstance frame) {
        return this.frames.get(frame.getIndex() - 1).getScore();
    }

    private boolean isFrameFinished(FrameInstance frame) {
        if (this.isFrameFinal(frame)) {
            return frame.getFirstRoll() != null && frame.getSecondRoll() != null && (
                    (frame.getFirstRoll().getPinsKnocked() + frame.getSecondRoll().getPinsKnocked()) >= 10 ?
                            frame.getThirdRoll() != null : true
            );
        }
        return frame.getFirstRoll() != null &&
                (frame.getFirstRoll().getPinsKnocked() == 10 || frame.getSecondRoll() != null);
    }

    private boolean qualifiesAsSpare(FrameInstance frame) {
        return frame.getFirstRoll() != null &&
                frame.getSecondRoll() != null &&
                frame.getSecondRoll().getPinsKnocked() > 0 &&
                frame.getFirstRoll().getPinsKnocked() + frame.getSecondRoll().getPinsKnocked() == 10;
    }

    private boolean qualifiesAsStrike(FrameInstance frame) {
        return frame.getFirstRoll() != null &&
                frame.getSecondRoll() == null &&
                frame.getFirstRoll().getPinsKnocked() == 10;
    }

    private boolean qualifiesForThirdRoll(FrameInstance frame) {
        return frame.getFirstRoll().getPinsKnocked() + frame.getSecondRoll().getPinsKnocked() >= 10;
    }

    private boolean isFrameFinal(FrameInstance frame) {
        return frame.getIndex() == 9;
    }
}
