package com.vm.model;

public class FrameScore {
    private Integer score;
    private Integer frameIndex;

    public FrameScore(Integer score, Integer frameIndex) {
        this.score = score;
        this.frameIndex = frameIndex;
    }

    public Integer getFrameIndex() {
        return frameIndex;
    }

    public void setFrameIndex(Integer frameIndex) {
        this.frameIndex = frameIndex;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }
}
