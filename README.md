# README #

This is a React/JavaEE application written by Igor Bejnarowicz in the recruitment process for VM.PL.
It is a bowling score table consisting of two modules: frontend, responsible for the general game state
(legal rolls, current active roll etc) and the backend, responsible for calculating the scoring of the frames.

## TL;DR ##

Maven is required to run the project. As long as it is present, running:

* `mvn clean install`

in the project root (`vm` folder) should do the trick.

Access the app at http://localhost:8080/

PS. If for some reason you the default WilfFly "Your application is running" screen appears, try emptying cache and
hard reloading the page.

## Thoughts ##

The most challenging part was definitely setting up the app in a deployment-like manner, so that everything runs on one port,
at the same time, under one maven command. Still, I'm not entirely sure whether my current approach to bundling frontend together
with the backend (which is: build the frontend and throw it into the backend .war file, then deploy the .war to WildFly)
is the correct approach. I would love some feedback on that.

The backend side of the app is *probably* not written in the most idiomatic JavaEE way it could, as there are really no interfaces,
for example, but the process of the implementation allowed me to get a much better grasp of the backend/JavaEE domain nonetheless.

### Possible improvements ###

* Tests! Unfortunately, currently there are none, due to lack of time. Most notably: reducer test in frontend and
EJB/service test on the backend.
* A proper eslint/prettier config in frontend. It's something I normally use all the time, but I got so caught up
in the implementation of the backend I completely disregarded the entire styling setup.
* Database integration and with that a possibility to play multiple games (like for example with friends) at once.
Also, persisting the game(s) state(s) would be nice.
