import {AppThunk, StoreState} from "../../rootReducers";
import {Dispatch} from "redux";
import {GameState, initialGameState, initialLegalRollValues} from "../../../model/GameState";
import {FrameState} from "../../../model/FrameState";
import {postResetGameRequest, postRoll} from "../../../utils/api";
import {FrameScore} from "../../../model/FrameScore";

const PERFORM_ROLL = 'vm/game/PERFORM_ROLL';
const ADVANCE_FRAME = 'vm/game/ADVANCE_FRAME';
const UPDATE_LEGAL_ROLL_VALUES = 'vm/game/UPDATE_LEGAL_ROLL_VALUES';
const FINISH_GAME = 'vm/game/FINISH_GAME';
const RESET_GAME_STATE = 'vm/game/RESET_GAME_STATE';

// API actions
const FETCH_UPDATED_FRAME_SCORES = 'vm/game/FETCH_UPDATED_FRAME_SCORES';
const FETCH_UPDATED_FRAME_SCORES_SUCCESS = 'vm/game/FETCH_UPDATED_FRAME_SCORES_SUCCESS';
const FETCH_UPDATED_FRAME_SCORES_FAILURE = 'vm/game/FETCH_UPDATED_FRAME_SCORES_FAILURE';


type PerformRollAction = {
    type: typeof PERFORM_ROLL;
    pinsKnocked: number;
}

type AdvanceFrameAction = {
    type: typeof ADVANCE_FRAME;
}

type UpdateLegalRollValuesAction = {
    type: typeof UPDATE_LEGAL_ROLL_VALUES;
    legalRollValues: number[];
}

type FinishGameAction = {
    type: typeof FINISH_GAME;
}

type ResetGameStateAction = {
    type: typeof RESET_GAME_STATE;
}

// API actions
type FetchUpdatedFrameScoresAction = {
    type: typeof FETCH_UPDATED_FRAME_SCORES;
}

type FetchUpdatedFrameScoresSuccessAction = {
    type: typeof FETCH_UPDATED_FRAME_SCORES_SUCCESS;
    frameScores: FrameScore[];
}

type FetchUpdatedFrameScoresFailureAction = {
    type: typeof FETCH_UPDATED_FRAME_SCORES_FAILURE;
    error: string;
}


export type GameActionsType = PerformRollAction | AdvanceFrameAction | UpdateLegalRollValuesAction | FinishGameAction | FetchUpdatedFrameScoresAction | FetchUpdatedFrameScoresSuccessAction | FetchUpdatedFrameScoresFailureAction | ResetGameStateAction;

export default (state: GameState = initialGameState, action?: GameActionsType): GameState => {
    switch (action?.type) {
        case PERFORM_ROLL: {
            const frames = [...state.frames];
            frames[frames.length - 1].rolls = [...frames[frames.length - 1].rolls, action.pinsKnocked];

            return {
                ...state,
                frames,
            };
        }
        case UPDATE_LEGAL_ROLL_VALUES: {
            const frames = [...state.frames];
            frames[frames.length - 1].legalRollValues = action.legalRollValues;

            return {
                ...state,
                frames,
            };
        }
        case ADVANCE_FRAME: {
            const emptyFrame: FrameState = {
                rolls: [],
                legalRollValues: [...initialLegalRollValues],
                score: undefined,
            };

            return {
                ...state,
                frames: [...state.frames, emptyFrame],
            };
        }
        case FINISH_GAME: {
            return {
                ...state,
                isGameFinished: true,
            };
        }
        case FETCH_UPDATED_FRAME_SCORES: {
            return {
                ...state,
                requestLoading: true,
            };
        }
        case FETCH_UPDATED_FRAME_SCORES_SUCCESS: {
            const frames = [...state.frames];
            action.frameScores.forEach(fs => {
                    const frame = frames.find((_r, idx) => idx === fs.frameIndex);
                    if (frame) {
                        frame.score = fs.score;
                    }
                }
            )

            return {
                ...state,
                frames,
                requestLoading: false,
            };
        }
        case FETCH_UPDATED_FRAME_SCORES_FAILURE: {
            return {
                ...state,
                requestLoading: false,
                requestError: action.error,
            }
        }
        case RESET_GAME_STATE: {
            return {
                ...initialGameState,
                frames: [{
                    rolls: [],
                    legalRollValues: [...initialLegalRollValues],
                }],
            };
        }
        default: {
            return {
                ...state,
            };
        }
    }
}

function performRoll(pinsKnocked: number): GameActionsType {
    return {
        type: PERFORM_ROLL,
        pinsKnocked,
    };
}

function advanceFrame(): GameActionsType {
    return {
        type: ADVANCE_FRAME,
    };
}

function updateLegalRollValues(legalRollValues: number[]): GameActionsType {
    return {
        type: UPDATE_LEGAL_ROLL_VALUES,
        legalRollValues,
    };
}

function finishGame(): GameActionsType {
    return {
        type: FINISH_GAME,
    };
}

function fetchUpdatedFrameScores(): GameActionsType {
    return {
        type: FETCH_UPDATED_FRAME_SCORES,
    };
}

function fetchUpdatedFrameScoresSuccess(frameScores: FrameScore[]): GameActionsType {
    return {
        type: FETCH_UPDATED_FRAME_SCORES_SUCCESS,
        frameScores,
    };
}

function fetchUpdatedFrameScoresFailure(error: string): GameActionsType {
    return {
        type: FETCH_UPDATED_FRAME_SCORES_FAILURE,
        error,
    };
}

function resetGameState(): GameActionsType {
    return {
        type: RESET_GAME_STATE,
    };
}

export function resetGame(): AppThunk {
    return async (dispatch: Dispatch): Promise<void> => {
        try {
            await postResetGameRequest();
            dispatch(resetGameState());
        } catch (e) {
            // TODO: Handle more gracefully than that
        }
    }
}

export function rollBall(pinsKnocked: number): AppThunk {
    return async (dispatch: Dispatch, getState: () => StoreState): Promise<void> => {
        const frames = () => getState().gameState.frames;
        const lastFrame = () => frames()[frames().length - 1];
        const rollsSum = () => lastFrame()?.rolls.reduce((sum, roll) => sum += roll, 0);

        dispatch(performRoll(pinsKnocked));

        try {
            dispatch(fetchUpdatedFrameScores());
            const response = await postRoll(pinsKnocked);
            const scores: FrameScore[] = await response.json();
            dispatch(fetchUpdatedFrameScoresSuccess(scores));
        } catch (e) {
            dispatch(fetchUpdatedFrameScoresFailure(e.message));
        }

        if (frames().length < 10) {
            if (rollsSum() === 10 || lastFrame()?.rolls.length === 2) {
                dispatch(advanceFrame());
            } else {
                dispatch(updateLegalRollValues(lastFrame()?.legalRollValues.slice(0, lastFrame()?.legalRollValues.length - pinsKnocked)));
            }
        } else { // Final frame
            if (lastFrame().rolls.length === 3 || (lastFrame().rolls.length === 2 && rollsSum() < 10)) {
                dispatch(finishGame());
            } else if ((lastFrame().rolls.length === 1 || lastFrame().rolls.length === 2) && rollsSum() % 10 === 0) {
                dispatch(updateLegalRollValues([...initialLegalRollValues]));
            } else {
                dispatch(updateLegalRollValues(lastFrame()?.legalRollValues.slice(0, lastFrame()?.legalRollValues.length - pinsKnocked)));
            }
        }
    }
}