import rootReducer from './rootReducers';
import thunk from 'redux-thunk';
import {composeWithDevTools} from 'redux-devtools-extension';
import {applyMiddleware, createStore} from "redux";

export default createStore(
    rootReducer,
    composeWithDevTools(applyMiddleware(thunk)),
);