import {combineReducers, Action} from 'redux';
import * as reducers from './ducks/index';
import {ThunkAction} from "redux-thunk";

const rootReducer = combineReducers(reducers);

export type StoreState = ReturnType<typeof rootReducer>;

export type AppThunk<ReturnType = void> = ThunkAction<
    ReturnType,
    StoreState,
    unknown,
    Action<string>
    >;

export default rootReducer;
