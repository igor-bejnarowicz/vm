import React from "react";
import styled from "styled-components";
import {useSelector} from "react-redux";
import {StoreState} from "../redux/rootReducers";
import {FrameState} from "../model/FrameState";
import Frame from "./Frame";

const Scoreboard: React.FC = () => {
    const frames = useSelector<StoreState, FrameState[]>((store) => store.gameState.frames);

    return <Container>
        {new Array(10).fill(null)
            .map((_e, idx) => <Frame key={idx}
                                     index={idx + 1}
                                     firstRoll={frames[idx]?.rolls[0]}
                                     secondRoll={frames[idx]?.rolls[1]}
                                     thirdRoll={frames[idx]?.rolls[2]}
                                     score={frames[idx]?.score} />)}
    </Container>
}

const Container = styled.div`
    display: flex;
    margin-bottom: 40px;
`;

export default Scoreboard;