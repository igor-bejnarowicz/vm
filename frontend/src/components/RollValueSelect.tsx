import React, {ChangeEvent, useState} from "react";
import styled from "styled-components";
import {useDispatch, useSelector} from "react-redux";
import {StoreState} from "../redux/rootReducers";
import {rollBall} from "../redux/ducks/game";
import {i18n} from "../utils/i18n";
import Button from "./Button";

const RollValueSelect: React.FC = () => {
    const dispatch = useDispatch();
    const [selectedOption, setSelectedOption] = useState(0);
    const isGameFinished = useSelector<StoreState, boolean>((state) => state.gameState.isGameFinished);
    const isRequestLoading = useSelector<StoreState, boolean>((state) => state.gameState.requestLoading);
    const legalRollValues = useSelector<StoreState, number[]>((state) => state.gameState.frames[state.gameState.frames.length - 1].legalRollValues);

    const handleSelectionChange = (e: ChangeEvent<HTMLSelectElement>) => {
        setSelectedOption(+e.target.value);
    }

    const onClick = () => {
        dispatch(rollBall(selectedOption));
    }

    return <Container>
        <Select value={selectedOption} onChange={handleSelectionChange} disabled={isGameFinished}>
            {legalRollValues.map(rv => <option key={rv} value={rv}>{rv}</option>)}
        </Select>
        <Button label={i18n.ROLL} onClick={onClick} disabled={isGameFinished || isRequestLoading} />
    </Container>
}

const Container = styled.div`
    display: flex;
    margin-bottom: 20px;
`;

const Select = styled.select`
    width: 80px;
    padding: 10px;
    border-radius: 15px;
    border: 3px solid lightgrey;
    outline: none;
    margin-right: 10px;
    
    :focus {
        border: 3px solid grey;
    }
`;

export default RollValueSelect;
