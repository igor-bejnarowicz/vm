import React from "react";
import StandardFrame from "./StandardFrame";
import FinalFrame from "./FinalFrame";
import styled from "styled-components";
import {Colors} from "../../utils/Colors";

type Props = {
    index: number;
    firstRoll?: number;
    secondRoll?: number;
    thirdRoll?: number;
    score?: number;
}

const Frame: React.FC<Props> = ({index, firstRoll, secondRoll, thirdRoll, score}) => {
    return <Container>
        <FrameNumber>{index}</FrameNumber>
        {index === 10 ?
        <FinalFrame firstRoll={firstRoll} secondRoll={secondRoll} thirdRoll={thirdRoll} score={score}  /> :
        <StandardFrame firstRoll={firstRoll} secondRoll={secondRoll} score={score}  />}
    </Container>;
}

const Container = styled.div`
`;
const FrameNumber = styled.div`
    color: ${Colors.lightGrey};
`;


export default Frame;