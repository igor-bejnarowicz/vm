import React from "react";
import styled from "styled-components";
import {Colors} from "../../utils/Colors";

type Props = {
    firstRoll?: number;
    secondRoll?: number;
    thirdRoll?: number;
    score?: number;
}

const FinalFrame: React.FC<Props> = ({firstRoll, secondRoll, thirdRoll, score}) => {
    const firstRollDisplayValue = firstRoll === 10 ? 'X' : firstRoll;
    const secondRollDisplayValue = secondRoll === 10 ? 'X' : (secondRoll || 0) > 0 && ((firstRoll || 0) + (secondRoll || 0) === 10) ? '/' : secondRoll;
    const thirdRollDisplayValue = thirdRoll === 10 ? 'X' : (thirdRoll || 0) > 0 && ((secondRoll || 0) + (thirdRoll || 0) === 10) ? '/' : thirdRoll;

    return <Container>
        <RollsContainer>
            <FirstRoll>{firstRollDisplayValue}</FirstRoll>
            <SecondRoll>{secondRollDisplayValue}</SecondRoll>
            <ThirdRoll>{thirdRollDisplayValue}</ThirdRoll>
        </RollsContainer>
        <Score>{score}</Score>
    </Container>
}

const Container = styled.div`
    border-left: 3px solid ${Colors.vmYellow};
    border-right: 3px solid ${Colors.vmYellow};
    color: ${Colors.vmYellow};
    width: 120px;
    background-color: ${Colors.vmGrey};
    border-radius: 8px;
    
`;

const RollsContainer = styled.div`
    display: flex;
`;

const Roll = styled.div`
    display: flex;
    flex-grow: 1;
    justify-content: center;
    align-items: center;
    height: 40px;
    
`;
const FirstRoll = Roll;
const SecondRoll = styled(Roll)`
    border-left: 2px solid ${Colors.vmYellow};
    border-bottom: 2px solid ${Colors.vmYellow};
    border-bottom-left-radius: 12px;
`;
const ThirdRoll = SecondRoll;

const Score = styled(Roll)`
    height: 60px;
`;

export default FinalFrame;