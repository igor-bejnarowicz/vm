import React from "react";
import styled from "styled-components";
import {Colors} from "../../utils/Colors";

type Props = {
    firstRoll?: number;
    secondRoll?: number;
    score?: number;
}

const StandardFrame: React.FC<Props> = ({firstRoll, secondRoll, score}) => {
    const firstRollDisplayValue = firstRoll === 10 ? '' : firstRoll;
    const secondRollDisplayValue = firstRoll === 10 ? 'X' : (firstRoll || 0) + (secondRoll || 0) === 10 ? '/' : secondRoll;

    return <Container>
        <RollsContainer>
            <FirstRoll>{firstRollDisplayValue}</FirstRoll>
            <SecondRoll>{secondRollDisplayValue}</SecondRoll>
        </RollsContainer>
        <Score>{score}</Score>
    </Container>
}

const Container = styled.div`
    border-left: 2px solid ${Colors.grey};
    border-right: 2px solid ${Colors.grey};
    width: 120px;
    background-color: ${Colors.lightGrey};
    border-radius: 8px;
    margin-right: 4px;
`;

const RollsContainer = styled.div`
    display: flex;
`;

const Roll = styled.div`
    display: flex;
    flex-grow: 1;
    justify-content: center;
    align-items: center;
    height: 40px;
    
`;
const FirstRoll = styled(Roll)``;
const SecondRoll = styled(Roll)`
    border-left: 2px solid ${Colors.grey};
    border-bottom: 2px solid ${Colors.grey};
    border-bottom-left-radius: 12px;
`;

const Score = styled(Roll)`
    height: 60px;
`;

export default StandardFrame;