import React from "react";
import styled from "styled-components";
import {Colors} from "../utils/Colors";

type Props = {
    label: string;
    onClick: () => void;
    disabled?: boolean;
}

const Button: React.FC<Props> = ({label, onClick, disabled}) => {
    return <Container onClick={onClick} disabled={disabled}>{label}</Container>;
}

const Container = styled.button`
    font-family: 'Raleway';
    border-radius: 15px;
    border: none;
    outline: none;
    padding: 15px;
    min-width: 120px;
    
    :hover {
        background-color: ${Colors.lightGrey};
    }
    
    :active {
        background-color: ${Colors.light};
    }
`;

export default Button;