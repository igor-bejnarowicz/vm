export enum i18n {
    DESCRIPTION = 'Hello. This is a React/JavaEE application written by Igor Bejnarowicz in the recruitment process for VM.PL',
    RESET = 'Reset game',
    ROLL = 'Roll!',
}