
export async function postRoll(pinsKnocked: number) {
    const response = await fetch('/api/postRoll', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({pinsKnocked})
    });
    return response;
}

export async function postResetGameRequest() {
    await fetch('/api/resetGame', {
        method: 'POST',
        headers: {
            'Content-Type': 'text/plain'
        },
    });
}