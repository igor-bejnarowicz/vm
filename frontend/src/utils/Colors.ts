export const Colors = {
    vmYellow: '#f1b72e',
    vmGrey: '#4e4e4e',
    grey: '#808080',
    lightGrey: '#d3d3d3',
    light: '#efefef',
}