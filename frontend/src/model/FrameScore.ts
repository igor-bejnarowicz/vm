export type FrameScore = {
    frameIndex: number;
    score: number;
}