import {FrameState} from "./FrameState";

export type GameState = {
    frames: FrameState[];
    isGameFinished: boolean;
    requestLoading: boolean;
    requestError: string;
}

export const initialLegalRollValues = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

export const initialGameState: GameState = {
    frames: [{
        rolls: [],
        legalRollValues: [...initialLegalRollValues],
    }],
    isGameFinished: false,
    requestLoading: false,
    requestError: '',
}