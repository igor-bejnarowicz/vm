export type FrameState = {
    rolls: number[];
    legalRollValues: number[];
    score?: number;
}