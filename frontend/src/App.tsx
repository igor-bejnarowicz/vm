import React from 'react';
import Scoreboard from "./components/Scoreboard";
import styled from "styled-components";
import RollValueSelect from "./components/RollValueSelect";
import LogoImg from "./assets/logo.png";
import {i18n} from "./utils/i18n";
import Button from "./components/Button";
import {useDispatch, useSelector} from "react-redux";
import {StoreState} from "./redux/rootReducers";
import {resetGame} from "./redux/ducks/game";

const App: React.FC = () => {
    const dispatch = useDispatch();
    const isAPIRequestLoading = useSelector<StoreState, boolean>((state) => state.gameState.requestLoading);

    const handleResetButtonClick = () => {
        dispatch(resetGame());
    };

  return (
        <StyledApp>
            <img src={LogoImg} alt="logo" />
            <Description>{i18n.DESCRIPTION}</Description>
            <RollValueSelect />
            <Scoreboard />
            <Button label={i18n.RESET} onClick={handleResetButtonClick} disabled={isAPIRequestLoading} />
        </StyledApp>
  );
}

const StyledApp = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    text-align: center;
    font-family: 'Raleway';
`;

const Description = styled.div`
    font-size: 40px;
    padding: 0 30px;
    margin-bottom: 100px;
    max-width: 1000px;
`;

export default App;
